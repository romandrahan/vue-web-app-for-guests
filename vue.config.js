const Dotenv = require('dotenv-webpack');
const fs = require('fs');

module.exports = {
  lintOnSave: false,
  devServer: {
    public: 'storybit.io',
    port: 443,
    https: {
      key: fs.readFileSync('./storybit.io.key'),
      cert: fs.readFileSync('./storybit.io.crt'),
    },
  },
  configureWebpack: {
    plugins: [
      new Dotenv(),
    ],
  },
};
